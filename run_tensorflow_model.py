from __future__ import absolute_import, division, print_function

import cv2
import argparse
import json
import numpy as np
import tensorflow as tf
import os

def preprocess_trivial(img):
	"""map image pixels to 0-1 and resize to 28x28
	@param img - the image
	@return resized_image with shape 28x28
	"""
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    resized_image = cv2.resize(gray, (28, 28)) 
    maxx, minn = 255., 0.
    #map to 0-1
    resized_image = (resized_image - minn) / (maxx - minn)
    return resized_image.reshape(-1)

def tf_cnn_slim_model(img, save_dir="modules/tensorflow_example/models/save"):
    """"get prediction for imag
    @param img - the image shape 28x28
    @param save_dir - the path to the models files meta and index
    @return [prediction, probability]
    """
    img = preprocess_trivial(img)
    
    loaded_Graph = tf.Graph()
    with tf.Session(graph=loaded_Graph) as sess:
        loader = tf.train.import_meta_graph(save_dir +'.meta')
        loader.restore(sess, save_dir)    
        # get tensors
        loaded_x = loaded_Graph.get_tensor_by_name('input:0')
        loaded_y = loaded_Graph.get_tensor_by_name('label:0')
        loaded_prob = loaded_Graph.get_tensor_by_name('probability:0')
        is_training = loaded_Graph.get_tensor_by_name('is_training:0')
        
        
        prob = sess.run(loaded_prob, 
                feed_dict = {loaded_x: np.array([img]),is_training:False})
        
        pre,pro = [np.amax(prob), np.argmax(prob)]
    	return float(pre),int(pro)

parser = argparse.ArgumentParser()
parser.add_argument("--path", help="the path to the image",
                    type=str,default='tensorflow/test7.jpg')

args = parser.parse_args()
out = {'status':'ok','prediction':-1,"probability":-1}

if os.path.isfile(args.path):
        image = cv2.imread(args.path)
        out['probability'], out['prediction'] = tf_cnn_slim_model(image)
        print (json.dumps(out))
else:
    print("{'status':'error','message':'no image found!'}")