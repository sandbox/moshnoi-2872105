# Tensorflow Example (Drupal 8)

* Provides an example of using tensorflow in Drupal.
* When an article is saved and has an image the module run a python script to get the prediction from tensorflow model


## Installation

* Install the modele as a normal module. 
* Requieres Tensorflow 1.0 and Opencv 3.0 

## Usage

Enable the module and add a new article with a image of a digits from mnist dataset.